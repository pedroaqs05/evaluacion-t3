#pragma checksum "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a149de354488c33af787f3dee6621ec28a7f2550"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_HistoriaClinica_Index), @"mvc.1.0.view", @"/Views/HistoriaClinica/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\_ViewImports.cshtml"
using Aquino_T3.WEB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\_ViewImports.cshtml"
using Aquino_T3.WEB.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a149de354488c33af787f3dee6621ec28a7f2550", @"/Views/HistoriaClinica/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"271a55f50e1be0ca05b1cc4fd8d2ff9625e5b686", @"/Views/_ViewImports.cshtml")]
    public class Views_HistoriaClinica_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1 class=\"text-center\">Lista de historias clinicas</h1>\r\n<div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n        <a href=\"/HistoriaClinica/Create\" class=\"btn btn-outline-info\">Nueva historia clinica</a>\r\n    </div>\r\n</div>\r\n<div class=\"card-deck\">\r\n");
#nullable restore
#line 13 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"card border-light bg-info my-3 text-light\">\r\n            <div class=\"card-body\">\r\n                <h5 class=\"card-title\">");
#nullable restore
#line 17 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                                  Write(item.NombreM);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4\">\r\n                        <label>Codigo: </label>\r\n                        ");
#nullable restore
#line 21 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Codigo);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Fecha de registro: </label>\r\n                        ");
#nullable restore
#line 25 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.FechaR.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Fehca de nacimiento: </label>\r\n                        ");
#nullable restore
#line 29 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.NacimientoM.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Sexo: </label>\r\n                        ");
#nullable restore
#line 33 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Sexo);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Especie: </label>\r\n                        ");
#nullable restore
#line 37 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Especie);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Raza: </label>\r\n                        ");
#nullable restore
#line 41 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Raza);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Tamaño: </label>\r\n                        ");
#nullable restore
#line 45 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Tamanio);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-12\">\r\n                        <label>Datos particulares: </label>\r\n                        ");
#nullable restore
#line 49 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.DatosParticulares);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                    </div>
                    <div class=""col-md-12 text-center  border-top"">
                        <h2 class=""card-subtitle my-3"">Datos del dueño</h2>
                    </div>
                    <div class=""col-md-4"">
                        <label>Nombre: </label>
                        ");
#nullable restore
#line 56 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.NombreDuenio);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Direcion: </label>\r\n                        ");
#nullable restore
#line 60 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.DirecionDuenio);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Telefono: </label>\r\n                        ");
#nullable restore
#line 64 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Telefono);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-4\">\r\n                        <label>Email: </label>\r\n                        ");
#nullable restore
#line 68 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
                   Write(item.Email);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 73 "D:\Calidadadadadadadadadadad\Aquino_T3.WEB\Aquino_T3.WEB\Views\HistoriaClinica\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
