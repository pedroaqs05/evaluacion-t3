﻿using Aquino_T3.WEB.Extensions;
using Aquino_T3.WEB.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Aquino_T3.WEB.Services
{

    public interface ICookieAuthService
    {
        public void SetHttpContext(HttpContext httpContext);
        public void SetSessionContext(User userdb);
        public void Login(ClaimsPrincipal claim);
    }
    public class CookieAuthService : ICookieAuthService
    {
        private HttpContext httpContext;

        public void Login(ClaimsPrincipal claim)
        {
            httpContext.SignInAsync(claim);
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        public void SetSessionContext(User userdb)
        {
            httpContext.Session.Set("LoggedUser", userdb);
        }
    }
}
