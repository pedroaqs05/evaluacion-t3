﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aquino_T3.WEB.Models
{
    public class HistoriaClinica
    {
        public int HistoriaId { get; set; }
        [Required]
        public string Codigo { get; set; }
        [Required]
        public DateTime FechaR { get; set; }
        [Required]
        public string NombreM { get; set; }
        [Required]
        public DateTime NacimientoM { get; set; }
        public string Sexo { get; set; }
        public string Especie { get; set; }
        public string Raza { get; set; }
        public string Tamanio { get; set; }
        public string DatosParticulares { get; set; }
        [Required]
        public string NombreDuenio { get; set; }
        [Required]
        public string DirecionDuenio { get; set; }
        [Required]
        public string Telefono { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}
