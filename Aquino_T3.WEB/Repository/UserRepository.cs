﻿using Aquino_T3.WEB.Models;
using Aquino_T3.WEB.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aquino_T3.WEB.Repository
{
    public interface IUserRepository
    {
        public User FindUserLogin(string userName, string password);
    }
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext context;
        public UserRepository(AppDbContext _context)
        {
            this.context = _context;
        }
        public User FindUserLogin(string userName, string password)
        {
            return context.Users.FirstOrDefault(o => o.UserName == userName && o.Password == password);
        }
    }
}
