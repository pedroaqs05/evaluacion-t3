﻿using Aquino_T3.WEB.Models;
using Aquino_T3.WEB.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aquino_T3.WEB.Repository
{
    public interface IHistoriaClinicaRepository
    {
        public bool ValidarCodigo(string codigo);
        public void Create(HistoriaClinica historiaClinica);
        public List<HistoriaClinica> Lista();
    }
    public class HistoriaClinicaRepository : IHistoriaClinicaRepository
    {
        private readonly AppDbContext context;
        public HistoriaClinicaRepository(AppDbContext _context)
        {
            this.context = _context;
        }
        public void Create(HistoriaClinica historiaClinica)
        {
            context.Historias.Add(historiaClinica);
            context.SaveChanges();
        }

        public List<HistoriaClinica> Lista()
        {
            return context.Historias.ToList();
        }

        public bool ValidarCodigo(string codigo)
        {
            if (context.Historias.Where(o => o.Codigo == codigo).Count() > 0)
            {
                return true;
            }
            return false;
        }
    }
}
