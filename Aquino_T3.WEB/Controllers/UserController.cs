﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Aquino_T3.WEB.Repository;
using Aquino_T3.WEB.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Aquino_T3.WEB.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository ur;
        private readonly ICookieAuthService cookieService;
        public UserController(IUserRepository _ur, ICookieAuthService _cookieService)
        {
            this.ur = _ur;
            this.cookieService = _cookieService;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string userName, string password)
        {
            var userdb = ur.FindUserLogin(userName, password);
            if (userdb == null)
            {
                ModelState.AddModelError("Usuario", "Usuario o contraseña incorrectos");
                return View();
            }
            cookieService.SetHttpContext(HttpContext);
            cookieService.SetSessionContext(userdb);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,userdb.UserName)
            };

            var Identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(Identity);

            cookieService.Login(principal);
            return RedirectToAction("Index", "HistoriaClinica");
        }
    }
}
