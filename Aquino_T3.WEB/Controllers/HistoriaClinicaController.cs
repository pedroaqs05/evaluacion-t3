﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aquino_T3.WEB.Models;
using Aquino_T3.WEB.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aquino_T3.WEB.Controllers
{
   
    public class HistoriaClinicaController : Controller
    {
        private readonly IHistoriaClinicaRepository hc;
        public HistoriaClinicaController(IHistoriaClinicaRepository _hc)
        {
            this.hc = _hc;
        }
        [Authorize]
        [HttpGet]
        public IActionResult Index()
        {
            var model = hc.Lista();
            return View(model);
        }
        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {
            return View(new HistoriaClinica());
        }
        [HttpPost]
        public IActionResult Create(HistoriaClinica historiaClinica)
        {
            if (hc.ValidarCodigo(historiaClinica.Codigo))
            {
                ModelState.AddModelError("Codigo", "El codigo ya esta registrado");
            }
            historiaClinica.FechaR = DateTime.Now;
            if (historiaClinica.NacimientoM > historiaClinica.FechaR)
            {
                ModelState.AddModelError("Fecha", "La fecha de nacimiento no puede ser mayor a la fecha actual");
            }
            if (ModelState.IsValid)
            {
                hc.Create(historiaClinica);
                return RedirectToAction("Index");
            }
            return View(historiaClinica);
        }
    }
}
