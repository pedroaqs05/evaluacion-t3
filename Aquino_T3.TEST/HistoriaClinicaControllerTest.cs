﻿using Aquino_T3.WEB.Controllers;
using Aquino_T3.WEB.Models;
using Aquino_T3.WEB.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aquino_T3.TEST
{
    class HistoriaClinicaControllerTest
    {
        [Test]
        public void CreateCorrecto()
        {
            var mockHistoriaClinicaReposiroty = new Mock<IHistoriaClinicaRepository>();
            mockHistoriaClinicaReposiroty.Setup(o => o.ValidarCodigo(It.IsAny<String>())).Returns(false);
            var controller = new HistoriaClinicaController(mockHistoriaClinicaReposiroty.Object);
            var historiaClinica = new HistoriaClinica();
            historiaClinica.Codigo = "1";
            historiaClinica.NacimientoM = DateTime.Now;
            historiaClinica.NombreM = "MascotaTest";
            historiaClinica.NombreDuenio = "DueñoTest";
            historiaClinica.DirecionDuenio = "DireccionTest";
            historiaClinica.Telefono = "987654321";
            var result = controller.Create(historiaClinica);
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void CreatIncorrecto()
        {
            var mockHistoriaClinicaReposiroty = new Mock<IHistoriaClinicaRepository>();
            mockHistoriaClinicaReposiroty.Setup(o => o.ValidarCodigo(It.IsAny<String>())).Returns(true);
            var controller = new HistoriaClinicaController(mockHistoriaClinicaReposiroty.Object);
            var historiaClinica = new HistoriaClinica();
            var result = controller.Create(historiaClinica);
            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void ListaCorrecta()
        {
            var mockHistoriaClinicaReposiroty = new Mock<IHistoriaClinicaRepository>();
            mockHistoriaClinicaReposiroty.Setup(o => o.Lista()).Returns(new List<HistoriaClinica>());
            var controller = new HistoriaClinicaController(mockHistoriaClinicaReposiroty.Object);
            var result = controller.Index();
            Assert.IsInstanceOf<ViewResult>(result);
        }
    }
}
