﻿using Aquino_T3.WEB.Controllers;
using Aquino_T3.WEB.Models;
using Aquino_T3.WEB.Repository;
using Aquino_T3.WEB.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aquino_T3.TEST
{
    class UserControllerTest
    {
        [Test]
        public void LoginCorrecto()
        {
            var mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository.Setup(o => o.FindUserLogin("admin", It.IsAny<String>()))
                .Returns(new User { UserName = "admin", Password = "admin" });
            var cookie = new Mock<ICookieAuthService>();
            var controller = new UserController(mockUserRepository.Object, cookie.Object);
            var view = controller.Login("admin", "admin");
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        [Test]
        public void LoginIncorrecto()
        {
            var mockUserRepository = new Mock<IUserRepository>();
            var cookie = new Mock<ICookieAuthService>();
            var controller = new UserController(mockUserRepository.Object, cookie.Object);
            var view = controller.Login("admin", "admin");
            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}
